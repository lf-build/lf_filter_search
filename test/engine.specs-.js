var should = require('should');
var assert = require('assert');

var engine = require('../lib/core/engine');
var config = require('./engine-config');

describe('Engine', function () {
  var root = engine(config);

  describe('Stage', function () {

    var dummyStage = root.stages['lead'];
    it('must set proper name', function () {
      dummyStage.name.should.equal('lead');
    });
    describe('$set', function () {
      it('must be defined', function () {
        assert.notEqual(dummyStage.$set, undefined);
      });
      it('must be able to set value under single level key');
      it('must be able to set value under multilevel key');
    });

    describe('$get', function () {
      it('must be defined', function () {
        assert.notEqual(dummyStage.$get, undefined);
      });
      it('must be able to get value under single level key');
      it('must be able to get value under multilevel key');
    });

    describe('$setStatus', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$setStatus, undefined);
      });
    });
    describe('$getStatus', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$getStatus, undefined);
      });
    });
    describe('$transform', function () {
      it('must defined', function () {
        assert.notEqual(dummyStage.$transform, undefined);
      });
    });


    describe('Fact', function () {
      var dummyFact = dummyStage.facts['person'];
      it('must define $set macro', function () {
        assert.notEqual(dummyFact.$set, undefined);
      });
      it('must define $get macro', function () {
        assert.notEqual(dummyFact.$get, undefined);
      });
      it('must define $setStatus macro', function () {
        assert.notEqual(dummyFact.$setStatus, undefined);
      });
      it('must define $getStatus macro', function () {
        assert.notEqual(dummyFact.$getStatus, undefined);
      });
      it('must define $transform macro', function () {
        assert.notEqual(dummyFact.$transform, undefined);
      });


      describe('actions', function () {
        var dummyAction = dummyFact.actions['set-number'];

        it('must define $set macro', function () {
          assert.notEqual(dummyAction.$set, undefined);
        });
        it('must define $get macro', function () {
          assert.notEqual(dummyAction.$get, undefined);
        });
        it('must define $setStatus macro', function () {
          assert.notEqual(dummyAction.$setStatus, undefined);
        });
        it('must define $getStatus macro', function () {
          assert.notEqual(dummyAction.$getStatus, undefined);
        });
        it('must define $transform macro', function () {
          assert.notEqual(dummyAction.$transform, undefined);
        });

        it('must execute without throwing exception');

        describe('activity', function () {
          var dummyActivity = dummyAction.activities['return'];

          it('must define $set macro', function () {
            assert.notEqual(dummyActivity.$set, undefined);
          });
          it('must define $get macro', function () {
            assert.notEqual(dummyActivity.$get, undefined);
          });
          it('must define $setStatus macro', function () {
            assert.notEqual(dummyActivity.$setStatus, undefined);
          });
          it('must define $getStatus macro', function () {
            assert.notEqual(dummyActivity.$getStatus, undefined);
          });
          it('must define $transform macro', function () {
            assert.notEqual(dummyActivity.$transform, undefined);
          });

          describe('$execute', function () {
            it('must execute without exception');

          });

        });
      });


    });

  });




});
