Please find sample saved filter request below:

```json

{
    "searchname": "savedFilterName",
    "searchFilterView": [
        {
            "StatusCode": ""
        }
    ],
    "inTagsSearch": [],
    "notInTagsSearch": [],
    "operator": "or",
    "projections": {
        "_id": 0,
        "field1": 1,
        "field2": 1,
        "field3": 1
    },
    "sorts": {
        "field1": -1
    }
}

```

Please find sample search application end point request below:

```json

{
    "searchFilterView": [
        { "field1": "" },
        { "field2": "" },
        { "field3": "" },
        { 
            "field4": 
            {
                "$gte":"2017-06-26T06:39:46.460Z","$lt":"2017-07-06T06:39:46.460Z"
            }
        }
    ],
    "inTagsSearch": [],
    "notInTagsSearch": [],
    "operator": "or",
    "projections": "",
    "sorts": {
        "field1":-1
    }
}

```
