const express = require('express');
const debug = require('debug')('ce_app_workflow:index');
const regexExp = /(\d{4})-(\d{2})-(\d{2})T((\d{2}):(\d{2}):(\d{2}))\.(\d{3})Z/i;

module.exports = {
    performFreeSearch: function (filterArgs) {
        let tenant = filterArgs.tenant;
        let searchFilterView = filterArgs.searchFilterView;
        let inTagsSearch = filterArgs.inTagsSearch;
        let notInTagsSearch = filterArgs.notInTagsSearch;
        let operator = filterArgs.operator;
        let projections = filterArgs.projections;
        let sorts = filterArgs.sorts;
        let filterCollection = filterArgs.filterCollection;
        let tagCollection = filterArgs.tagCollection;
        let query;

        const filter = [{ // Join state -- to map tags with application numbers
            "$lookup": {
                "from": tagCollection,
                localField: "ApplicationNumber",
                foreignField: "ApplicationNumber",
                as: "tags"
            }
        }];

        if (projections) {
            filter.push(Object.assign(projections, { // Reduce the field to just array of array of string, plus merge with provided projection
                "$project": {
                    tags: "$tags.Tags.TagName"
                }
            }));
        } else {
            filter.push({ // Reduce the field to just array of array of string (add field as not projections were provided)
                "$addFields": {
                    tags: "$tags.Tags.TagName"
                }
            });
        }

        if (sorts) {
            filter.push({ $sort: sorts });
        }
        filter.push({ // Unwind external array as ApplicationNumber has 1 to 1 mapping in tags.
            $unwind: {
                path: "$tags",
                preserveNullAndEmptyArrays: true
            }
        });

        if (!searchFilterView) {
            searchFilterView = {};
        }
        searchFilterView.TenantId = tenant;

        filter.push({ $match: searchFilterView });

        return filterArgs
            .store
            .dbAction(function* (db) {
                const { count } = (yield db.collection(filterCollection).aggregate([
                    ...filter, {
                        $count: "count"
                    }
                ]).toArray())[0];

                return {
                    query: db
                        .collection(filterCollection)
                        .aggregate(filter),
                    count
                };
            });
    },
    performSearch: function (filterArgs) {
        let tenant = filterArgs.tenant;
        let searchFilterView = filterArgs.searchFilterView;
        let inTagsSearch = filterArgs.inTagsSearch;
        let notInTagsSearch = filterArgs.notInTagsSearch;
        let operator = filterArgs.operator;
        let projections = filterArgs.projections;
        let sorts = filterArgs.sorts;
        let filterCollection = filterArgs.filterCollection;
        let tagCollection = filterArgs.tagCollection;
        let entity = filterArgs.entity;
        let query;
        searchFilterView.forEach(function (obj) {
            if (regexExp.test(JSON.stringify(obj))) {
                var keys = Object.keys(obj);
                var dateRageObj = obj[keys[0]];
                var subKeys = Object.keys(dateRageObj);
                subKeys.forEach(function (item) {
                    dateRageObj[item] = new Date(dateRageObj[item]);
                });
            }
        });
        return filterArgs
            .store
            .dbAction(function* (db) {
                let tagFilter = [];
                let appsList = [];
                if (inTagsSearch && inTagsSearch.length) {
                    tagFilter.push({
                        Tags: {
                            $elemMatch: {
                                TagName: {
                                    $in: inTagsSearch
                                }
                            }
                        }
                    });
                }
                if (notInTagsSearch && notInTagsSearch.length) {
                    tagFilter.push({
                        Tags: {
                            $not: {
                                $elemMatch: {
                                    TagName: {
                                        $in: notInTagsSearch
                                    }
                                }
                            }
                        }
                    });
                }
                if (tagFilter.length) {
                    if (operator.toLowerCase() == 'and') {
                        query = db
                            .collection(tagCollection)
                            .find({
                                $and: [{
                                    TenantId: tenant
                                }, {
                                    $and: tagFilter
                                }]
                            }, {
                                    ApplicationNumber: 1,
                                    _id: 0
                                });
                    } else {
                        query = db
                            .collection(tagCollection)
                            .find({
                                $and: [{
                                    TenantId: tenant
                                }, {
                                    $or: tagFilter
                                }]
                            });
                    }

                    let intermediateResults;
                    try {
                        intermediateResults = yield query.toArray();
                        appsList = intermediateResults.map(function (a) {
                            return a.ApplicationNumber;
                        });
                    } catch (e) {
                        return undefined;
                    }
                    if (appsList && appsList.length) {
                        // Try to find if there is Application Number query in searchFilterView
                        let applicationNumberQuery;
                        if (searchFilterView.length) {
                            for (var i = 0; i < searchFilterView.length; i++) {
                                if (searchFilterView[i].ApplicationNumber) {
                                    applicationNumberQuery = searchFilterView[i];
                                    searchFilterView.splice(i, 1);
                                }
                            }
                        }

                        // If we found Application Number query then update apps List
                        if (applicationNumberQuery) {
                            appsList.push(applicationNumberQuery.ApplicationNumber);
                        }

                        searchFilterView.push({
                            ApplicationNumber: {
                                $in: appsList
                            }
                        });

                    }
                }
                if (searchFilterView && !searchFilterView.length) {
                    return db
                        .collection(filterCollection)
                        .find({
                            1: 0
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                }

                if (operator.toLowerCase() == 'and') {
                    return db
                        .collection(filterCollection)
                        .find({
                            $and: [{
                                TenantId: tenant
                            }, {
                                $and: searchFilterView
                            }]
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                } else {
                    return db
                        .collection(filterCollection)
                        .find({
                            $and: [{
                                TenantId: tenant
                            }, {
                                $or: searchFilterView
                            }]
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                }
            });
    },
    performLoanSearch: function (filterArgs) {
        let tenant = filterArgs.tenant;
        let searchFilterView = filterArgs.searchFilterView;
        let inTagsSearch = filterArgs.inTagsSearch;
        let notInTagsSearch = filterArgs.notInTagsSearch;
        let operator = filterArgs.operator;
        let projections = filterArgs.projections;
        let sorts = filterArgs.sorts;
        let filterCollection = filterArgs.filterCollection;
        let tagCollection = filterArgs.tagCollection;
        let query;
        searchFilterView.forEach(function (obj) {
            if (regexExp.test(JSON.stringify(obj))) {
                var keys = Object.keys(obj);
                var dateRageObj = obj[keys[0]];
                var subKeys = Object.keys(dateRageObj);
                subKeys.forEach(function (item) {
                    dateRageObj[item] = new Date(dateRageObj[item]);
                });
            }
        });
        return filterArgs
            .store
            .dbAction(function* (db) {
                let tagFilter = [];
                let appsList = [];
                if (inTagsSearch && inTagsSearch.length) {
                    tagFilter.push({
                        Tags: {
                            $elemMatch: {
                                TagName: {
                                    $in: inTagsSearch
                                }
                            }
                        }
                    });
                }
                if (notInTagsSearch && notInTagsSearch.length) {
                    tagFilter.push({
                        Tags: {
                            $not: {
                                $elemMatch: {
                                    TagName: {
                                        $in: notInTagsSearch
                                    }
                                }
                            }
                        }
                    });
                }
                if (tagFilter.length) {
                    if (operator.toLowerCase() == 'and') {
                        query = db
                            .collection(tagCollection)
                            .find({
                                $and: [{
                                    TenantId: tenant
                                }, {
                                    $and: tagFilter
                                }]
                            }, {
                                    LoanNumber: 1,
                                    _id: 0
                                });

                    } else {
                        query = db
                            .collection(tagCollection)
                            .find({
                                $and: [{
                                    TenantId: tenant
                                }, {
                                    $or: tagFilter
                                }]
                            });
                    }

                    let intermediateResults;
                    try {
                        intermediateResults = yield query.toArray();
                        appsList = intermediateResults.map(function (a) {
                            return a.LoanNumber;
                        });
                    } catch (e) {
                        return undefined;
                    }
                    if (appsList && appsList.length) {
                        // Try to find if there is Application Number query in searchFilterView
                        let applicationNumberQuery;
                        if (searchFilterView.length) {
                            for (var i = 0; i < searchFilterView.length; i++) {
                                if (searchFilterView[i].LoanNumber) {
                                    applicationNumberQuery = searchFilterView[i];
                                    searchFilterView.splice(i, 1);
                                }
                            }
                        }

                        // If we found Application Number query then update apps List
                        if (applicationNumberQuery) {
                            appsList.push(applicationNumberQuery.LoanNumber);
                        }
                        searchFilterView.push({
                            LoanNumber: {
                                $in: appsList
                            }
                        });
                    }
                }
                if (searchFilterView && !searchFilterView.length) {
                    return db
                        .collection(filterCollection)
                        .find({
                            1: 0
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                }

                if (operator.toLowerCase() == 'and') {
                    return db
                        .collection(filterCollection)
                        .find({
                            $and: [{
                                TenantId: tenant
                            }, {
                                $and: searchFilterView
                            }]
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                } else {
                    return db
                        .collection(filterCollection)
                        .find({
                            $and: [{
                                TenantId: tenant
                            }, {
                                $or: searchFilterView
                            }]
                        }, projections || undefined)
                        .sort(sorts || {
                            _id: 1
                        });
                }
            });
    },
    performSearchMerged: function (filterArgs) {
        let tenant = filterArgs.tenant;
        let searchFilterView = filterArgs.searchFilterView || [];
        let inTagsSearch = filterArgs.inTagsSearch || [];
        let notInTagsSearch = filterArgs.notInTagsSearch || [];
        let operator = filterArgs.operator || 'or';
        let projections = filterArgs.projections || { '_id': 1 };
        let sorts = filterArgs.sorts || { '_id': -1 };
        let filterCollection = filterArgs.filterCollection;
        let entity = filterArgs.entity;

        searchFilterView.forEach(function (obj) {   //Converting String Date to Date Object for MongoDB
            if (regexExp.test(JSON.stringify(obj))) {
                var keys = Object.keys(obj);
                var dateRageObj = obj[keys[0]];
                var subKeys = Object.keys(dateRageObj);
                subKeys.forEach(function (item) {
                    dateRageObj[item] = new Date(dateRageObj[item]);
                });
            }
        });

        if (inTagsSearch.length > 0) {
            searchFilterView.push({
                "Tags": {
                    $elemMatch: {
                        TagName: {
                            $in: inTagsSearch
                        }
                    }
                }
            });
        }
        if (notInTagsSearch.length > 0) {
            searchFilterView.push({
                "Tags": {
                    $not: {
                        $elemMatch: {
                            TagName: {
                                $in: notInTagsSearch
                            }
                        }
                    }
                }
            });
        }
        let filter = {};
        if (operator.toLowerCase() === 'and') {
            filter.$and = [{ TenantId: tenant }, ...searchFilterView];
        } else if (operator.toLowerCase() === 'or') {
            filter.$and = [{ TenantId: tenant }, { $or: searchFilterView }];
        } else {
            throw new Error('search filter: operator is not defined');
        }

        return filterArgs.store.dbAction(function* (db) {
            return db.collection(filterCollection)
                .find(filter, projections)
                .sort(sorts);
        });
    },
    findOne: (store, collection, fieldName, value) => {
        return store.dbAction(function* (db) {
            return db.collection(collection).findOne({
                [fieldName]: value
            });
        });
    },
    mergeTagsWithLoanFilterRecord: function (store, intermediateResults, tagCollection) {
        return store
            .dbAction(function* (db) {
                for (var i = 0, len = intermediateResults.length; i < len; i++) {
                    var tags = yield db
                        .collection(tagCollection)
                        .aggregate([{
                            $unwind: {
                                path: '$Tags.Tags',
                                preserveNullAndEmptyArrays: true
                            }
                        },
                        {
                            $match: { LoanNumber: intermediateResults[i].LoanNumber }
                        },
                        {
                            $project: { Tags: 1, _id: 0 }
                        }
                        ]).toArray();

                    if (tags.length) {
                        intermediateResults[i] = Object.assign(intermediateResults[i], { Tags: tags[0].Tags });
                    }
                }
                return intermediateResults;
            });
    },
    mergeTagsWithFilterRecord: function (store, intermediateResults, tagCollection) {
        return store
            .dbAction(function* (db) {
                for (var i = 0, len = intermediateResults.length; i < len; i++) {
                    var tags = yield db
                        .collection(tagCollection)
                        .aggregate([{
                            $unwind: {
                                path: '$Tags.Tags',
                                preserveNullAndEmptyArrays: true
                            }
                        },
                        {
                            $match: { ApplicationNumber: intermediateResults[i].ApplicationNumber }
                        },
                        {
                            $project: { Tags: 1, _id: 0 }
                        }
                        ]).toArray();

                    if (tags.length) {
                        intermediateResults[i] = Object.assign(intermediateResults[i], { Tags: tags[0].Tags });
                    }
                }
                return intermediateResults;
            });
    }
};