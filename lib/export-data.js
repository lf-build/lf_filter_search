const co = require('co');
const json2csv = require('json2csv');
/*
   Expects req.q (q should be a base query), req.savedSearchName
*/
module.exports = function (req, res, next) {
    if (req.q) {
        co(function* () {
            const recordsCount = yield req.q.count();
            return recordsCount <= 0
                ? Promise.resolve(undefined)
                : req.q.toArray();
        }).then(function (records) {
            if (!records || records.length === 0) {
                res.status(404).json({ message: 'No records available.' });
                return;
            }
            try {
                let exportFields = req.exportFields;
                let exportFieldNames = req.exportFieldNames;
                if (!exportFields || !exportFieldNames || exportFields.length === 0) {
                    res.status(404).json({ message: 'No export fields defined.' });
                    return;
                }

                let savedSearchName = req.savedSearchName ? req.savedSearchName : "applications";
                let result = json2csv({ data: records, fields: exportFields, fieldNames: exportFieldNames });
                let fileNameHeader = "attachment; filename=" + savedSearchName + ".csv";
                res.setHeader('Content-disposition', fileNameHeader);
                res.set('Content-Type', 'text/csv');
                res.status(200).send(result);
            } catch (err) {
                res.status(404).json({ message: 'Failed to parse json response into csv' });
                return;
            }
        });
    } else {
        next();
    }
};