const co = require('co');
/*
   Expects page, pageSize in query string and req.q (q should be a base query)
*/
module.exports = function (req, res, next) {
    if (req.q) {
        co(function* () {
            const page = req.query.page && parseInt(req.query.page) || 1;
            const pageSize = req.query.pageSize && parseInt(req.query.pageSize) || 10;
            const recordsCount = req.count ? req.count : yield req
                .q
                .count();
            const totalPages = Math.ceil(recordsCount / pageSize);
            res.set('X-TOTAL-COUNT', recordsCount);
            res.set('X-CURRENT-PAGE', page);
            res.set('X-TOTAL-PAGES', totalPages);
            res.set('X-PAGE-SIZE', pageSize);
            return totalPages < page
                ? Promise.resolve(undefined)
                : req
                    .q
                    .skip((page - 1) * pageSize)
                    .limit(pageSize)
                    .toArray();
        })
            .then(function (records) {
                if (!records) {
                    res
                        .status(404)
                        .json({ message: 'Page you are looking for does not exists' });
                    return;
                }
                res
                    .status(200)
                    .json(records);
            });
    } else {
        next();
    }
};