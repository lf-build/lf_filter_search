const _ = require('lodash');
const debug = require('lf-core/lib/misc/debug')('storage:persistent');
const { dbAction } = require('lf-core-backend-extensions/lib/storage');

const read = ({store, workflowId}) => db  => db.collection(store).findOne({ _id: workflowId});
const write = ({store, workflowId, value}) => db => db.collection(store)
  .updateOne(
    { _id: workflowId },
    value,
    { upsert: true, w: 1 }
  );

const hooks = {
  read() {

  }
};

module.exports = {
  hooks,
  get (...keys) {
    let pointer = {};

    switch(keys.length){
      case 2:
        {
          const [store, workflowId] = keys;
          pointer = {
            store,
            workflowId
          };
        }
        break;
      case 3:
        {
          const [store, stage, workflowId] = keys;
          pointer = {
            store,
            stage,
            workflowId
          };
        }
        break;
      default:
        if(keys.length < 2) {
          return new Promise((resolve, reject) => reject({'message': 'Item not found'}));
        }
        {
          const [store, stage, fact, workflowId, ...restOfKeys] = keys;
          pointer = {
            store,
            stage,
            fact,
            workflowId,
            restOfKeys
          };
        }
    }

    debug('reading', pointer);

    return new Promise((resolve, reject) => {
      dbAction(read(pointer))
        .then( data => {
            const {store, workflowId, fact, stage, restOfKeys} = pointer;
            try{
              hooks.read({store, workflowId, value: data});
            }catch(e) {
              debug('failed to call read hook', e);
            }
            const item = data
              ? ( stage ? [...(fact ? [stage, fact] : [stage]), ...(restOfKeys || [])]
              .reduce((a,c) => a ? a[c] : a, data) : data)
              : undefined;

            item ? resolve(Object.assign(item, { _id: undefined })) : reject(item);
        })
        .catch(reject);
    });
  },

  set(...args) {
    let [[store, stage, fact, workflowId, ...restOfKeys],[value]] =  _.chunk(args, args.length -1);
    const pointer = {
      store,
      stage,
      fact,
      workflowId,
      restOfKeys
    };

    debug(store, stage, fact, workflowId, restOfKeys, value);

    return new Promise((resolve, reject) => {

      dbAction(read(pointer))
        .then( data => {
          data = data || { _id: workflowId, [stage]: {[fact]: {} } };

          //add stage, fact at the begning fo restOfKeys
          restOfKeys = [stage, fact, ...restOfKeys];

          restOfKeys.reduce((a, c, i) => {
            if(i === restOfKeys.length - 1){
              return a[c] = value;
            }

            return a[c] || (a[c] = {});
          }, data);
          return dbAction(write(Object.assign(pointer, {value: data})))
            .then( () => resolve(value));
        })
        .catch(reject);

    });

  }
}
