var debug = require('lf-core/lib/misc/debug')('storage');
var _ = require('lodash');

const simulation_mode = process.env.SIMULATION_MODE === 'true';
if(simulation_mode){
  debug('Running in simulation mode.');
  debug('NOTE: All data is volatile');

  module.exports = require('./in-memory.js');
  return;
}

debug('Loading other');

var _ = require('lodash');
var cache = require('./cache');
var db = require('./persistent');

db.hooks.read = cache.write;

module.exports  = {
  get(...keys) {
    return new Promise(function (resolve, reject) {
      cache.get.apply(cache, keys)
        .then(value => {
          debug('got', keys, value);
          resolve(value);
        })
        .catch(err => {
          debug('cache miss for', keys, err)
          db.get.apply(db, keys)
            .then(value => {
              resolve(value);
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  },
  set(...args) {
    debug('setting', args);
    let [keys, [value]] = _.chunk(args, args.length -1 );
    return Promise.all([cache.set.apply(cache, args), db.set.apply(cache, args)])
      .then(() => {
        // debug('value set', args, value.length ? value[0] : undefined)
        debug(`setting "${keys.join('.')}" = ${JSON.stringify(value)}`);
        return  value.length ? value[0] : undefined;
      })
      .catch((err) => {
        debug('failed to set', args, err);
      });
  }
}
