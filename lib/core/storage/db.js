const mongoConnectionString = process.env.MONGODB_URI || "mongodb://localhost:27017/{{db}}";
const co = require('co');
const MongoClient = require('mongodb').MongoClient;
const debug = require('lf-core/lib/misc/debug')('storage:driver:persistent');

let dbs = {};
module.exports = (dbname) => dbAction => co(function*() {
    if (!dbs[dbname]) {
        debug('New connection created');
        dbs[dbname] = yield MongoClient.connect(mongoConnectionString.replace(/{{db}}/g, dbname));
        dbs[dbname].on("close", function(error) {
            debug('Connection was closed because of ', error);
            delete dbs[dbname];
        });
    }
    try {
        const result = yield dbAction(dbs[dbname]);
        return result;
    } catch (e) {
        console.log('error: ', JSON.stringify(e));
    }
});