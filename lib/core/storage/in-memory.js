var Promise = require('bluebird');
var _ = require('lodash');
var debug = require('lf-core').debug('storage:in-memory');
var dummyStore = {};

module.exports = {
   get (...keys) {
    let leafKey = _.last(keys);
    let extractedValue = undefined;

     if(keys.length === 2){
       const [storeType, workflowId] = keys;
       const val = dummyStore[storeType];

       extractedValue = {};

      // obj.stage
       Object.keys(val).forEach(stage => {
          let objx = {};
          // obj.stage.fact
          Object.keys(val[stage]).forEach(fact => {
             objx[fact] = val[stage][fact][workflowId];
          });

          extractedValue[stage] = objx;
       });
     }

     if(keys.length === 3){
       const [storeType, stage, workflowId] = keys;
       const val = dummyStore[storeType];

       extractedValue = {};
       // obj.stage.fact
       Object.keys(val[stage]).forEach(fact => {
         extractedValue[fact] = val[stage][fact][workflowId];
       });
     }

     return new Promise(function (resolve, reject) {
       if(extractedValue) {
         resolve(extractedValue);
         return;
       } else if(keys.length === 2 || keys.length === 3){
         reject({ 'not found': keys.join('') });
       }
       var result = _.reduce(keys, (a, c, index) => {
         if(!a){
           return a;
         }

         if(index === keys.length - 1 && c === leafKey){
           return a[c];
         }else {
           if(typeof a[c] !== 'object'){
             return undefined;
           }
         }

         return a[c] ? a[c] : a[c] = { };
       }, dummyStore);

       if(!(_.isUndefined(result) || _.isNull(result))){
         resolve(result);
       }else {
         reject({ 'not found': keys.join('.') });
       }
     });
   },

  set(...args) {
    return new Promise(function (resolve, reject) {
      let [keys, [value]] = _.chunk(args, args.length -1 );
      let leafKey = _.last(keys);

      _.reduce(keys, (a, c, index) => {
        return a[c] && typeof a[c] === 'object'
          ? a[c] = (index === keys.length -1 && c === leafKey ? value : a[c])
          : a[c] = (
            index === keys.length -1 && c === leafKey
            ? value
            : { }
          );
      }, dummyStore);

      debug(`setting "${keys.join('.')}" = ${JSON.stringify(value)}`);
      resolve(value);
    });
  }
};
