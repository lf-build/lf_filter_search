const express = require('express');
const router = express.Router();
const debug = require('debug')('ce_app_workflow:index');
const paginate = require('../lib/paginate');
const exportData = require('../lib/export-data');
const appFilters = require('../lib/AppFilters/appfilters');
const co = require('co');
const store = require('../lib/core/storage/db');
const { pick, forEach } = require('ramda');

const normalize = (obj, replace) => {
    if (obj instanceof Object && !(obj instanceof Array)) {
        return Object.keys(obj).reduce((a, c) => Object.assign(a, {
            [replace(c)]: normalize(obj[c], replace)
        }), {})
    }
    return obj;
}

router.param('entity', (req, res, next, entity) => {
    if (entity) {
        req.api.get(`${process.env.API_CONFIGURATION_URL}/filter-search`)
            .then((response) => response.body)
            .then((settings) => {
                if (!settings[entity]) {
                    next(new Error(`${entity} is not configured.`));
                    return;
                }

                const entitySet = settings[entity];
                var db = "";
                var filterCollection = "";
                var tagCollection = "";
                forEach((e) => {

                    if (e.type == "none")
                        return;
                    else if (e.type == "tag") {
                        tagCollection = e.collection;
                    } else if (e.type == "filter") {
                        db = e.db;
                        filterCollection = e.collection;
                    }
                }, entitySet);
                req.store.dbAction = store(db);
                req.tagCollection = tagCollection;
                req.filterCollection = filterCollection;
                next();
            })
            .catch(next);
    } else {
        next();
    }
});

router.param('e', (req, res, next, entity) => {
    if (entity) {
        req.api.get(`${process.env.API_CONFIGURATION_URL}/filter-search`)
            .then((response) => response.body)
            .then((settings) => {
                if (!settings[entity]) {
                    next(new Error(`${entity} is not configured.`));
                    return;
                }
                const entitySet = settings[entity];
                forEach((e) => {
                    if (e.type != "none")
                        return;

                    const filterObj = pick(e.fields, req.body);
                    e.data = store(e.db)(function* (db) {
                        return db.collection(e.collection).find(filterObj).project({ _t: 0 }).toArray();
                    });
                }, entitySet);

                Promise.all(entitySet.map(e => e.data))
                    .then((collections) => {
                        req.data = entitySet.map((e, index) => {

                            e.data = collections[index];
                            return e;
                        })
                            .reduce((a, c) => {
                                return Object.assign(a, {
                                    [c.collection]: c.data
                                });
                            }, {});
                    }).then(() => next());
            })
            .catch(next);
    } else {
        next();
    }
});

router.post('/:entity/SaveSearch/', function (req, res, next) {
    let tenant = req.user.tenant;
    let SearchCriteria = req.body;
    let entity = req.params.entity;
    let query;
    let shouldUpdate = false;
    if (SearchCriteria) {
        if (!SearchCriteria.searchname) {
            res
                .status(404)
                .json({ message: 'No search name defined' });
            return;
        }
        req.store.dbAction(function* (db) {
            try {
                db.collection("saved-search")
                    .updateOne({ searchname: SearchCriteria.searchname, TenantId: tenant }, {
                        $set: {
                            searchFilterView: SearchCriteria.searchFilterView,
                            inTagsSearch: SearchCriteria.inTagsSearch,
                            notInTagsSearch: SearchCriteria.notInTagsSearch,
                            operator: SearchCriteria.operator,
                            projections: SearchCriteria.projections,
                            sorts: SearchCriteria.sorts,
                            replaceChar: SearchCriteria.replaceChar,
                            replacementChars: SearchCriteria.replacementChars,
                            exportFields: SearchCriteria.exportFields,
                            exportFieldNames: SearchCriteria.exportFieldNames
                        }
                    }, {
                            upsert: true
                        });

                res
                    .status(200)
                    .json({ message: 'Search with name ' + SearchCriteria.searchname + ' created/updated successfully' });
                return;
            } catch (e) {
                res
                    .status(404)
                    .json({ message: e.error });
                return;
            }
        });
    }
});

router.get('/:entity/byTags/:tags', [function (req, res, next) {
    let tagsArray;
    let tenant = req.user.tenant;
    let entity = req.params.entity;
    if (req.params.tags) {
        tagsArray = req.params.tags.match(/[^-]+/g);
    }

    req.store.dbAction(function* (db) {
        const qry = db.collection(req.tagCollection)
            .find({
                Tags: {
                    $elemMatch: {
                        TagName: {
                            $in: tagsArray
                        }
                    }
                },
                TenantId: tenant
            }, {
                    ApplicationNumber: 1,
                    _id: 0
                });

        const tagResults = yield qry.toArray();
        let appsToReturn = tagResults.map(function (a) { return a.ApplicationNumber; });

        return db.collection(req.filterCollection)
            .find({ ApplicationNumber: { $in: appsToReturn } });

    })
        .then(function (q) {
            req.q = q;
            next();
        });
}, paginate]);

router.post('/:entity/search-applications-free', [function (req, res, next) {
    let entity = req.params.entity;
    appFilters.performFreeSearch(Object.assign(req.body, { tenant: req.user.tenant, store: req.store, filterCollection: req.filterCollection, tagCollection: req.tagCollection }))
        .then(function (q) {
            if (q) {
                req.q = q.query;
                req.count = q.count;
                next();
            } else {
                res
                    .status(404)
                    .json({ message: 'Search failed for tenant #' + req.user.tenant });
            }
        });
}, paginate]);

router.post('/:entity/search-applications', [function (req, res, next) {
    let entity = req.params.entity;
    appFilters.performSearch(Object.assign(req.body, { tenant: req.user.tenant, store: req.store, entity: entity, filterCollection: req.filterCollection, tagCollection: req.tagCollection }))
        .then(function (q) {
            if (q) {
                req.q = q;
                next();
            } else {
                res
                    .status(404)
                    .json({ message: 'Search failed for tenant #' + req.user.tenant });
            }
        });
}, paginate]);

router.post('/:entity/search-loans', [function (req, res, next) {
    let entity = req.params.entity;
    appFilters.performLoanSearch(Object.assign(req.body, { tenant: req.user.tenant, store: req.store, entity: entity, filterCollection: req.filterCollection, tagCollection: req.tagCollection }))
        .then(function (q) {
            if (q) {
                req.q = q;
                next();
            } else {
                res
                    .status(404)
                    .json({ message: 'Search failed for tenant #' + req.user.tenant });
            }
        });
}, paginate]);

router.post('/:entity/Search/:savedSearchName', [function (req, res, next) {
    let tenant = req.user.tenant;
    let savedSearchName = req.params.savedSearchName;
    let noPage = req.query.noPage;
    let sortOverride = req.body.sorts;
    let searchFilterViewOverride = req.body.searchFilterView;
    let searchParams;
    let entity = req.params.entity;
    req.store.dbAction(function* (db) {
        if (savedSearchName && savedSearchName.length) {
            return yield db.collection('saved-search').findOne({ searchname: savedSearchName, TenantId: tenant });
        } else {
            res.status(404).json({ message: 'Missing parameter - Saved Search Name' });
            return;
        }
    }).then(function (searchParams) {
        if (searchParams) {
            let replacementChars = searchParams.replacementChars;
            let replaceChar = searchParams.replaceChar;
            const replace = (str) => Object.keys(replacementChars).reduce((a, c) => a.replace(new RegExp(c, 'g'), replacementChars[c]), str);

            if (searchFilterViewOverride) { searchParams.searchFilterView = searchFilterViewOverride; }
            if (replaceChar) {
                searchParams.searchFilterView = searchParams.searchFilterView.reduce((a, c) => {
                    return [...a, normalize(c, replace)];
                }, []);
            }
            if (sortOverride) { searchParams.sorts = sortOverride; }
            appFilters.performSearchMerged(Object.assign(searchParams, { tenant: req.user.tenant, store: req.store, entity: entity, filterCollection: req.filterCollection })).then(function (q) {
                if (q) {
                    req.q = q;
                    next();
                } else {
                    res.status(404).json({ message: 'Search failed for tenant #' + req.user.tenant });
                }
            });
        } else {
            res.status(404).json({ message: 'Search ' + savedSearchName + ' does not exist for tenant #' + tenant });
            return;
        }
    });
}, paginate]);

router.post('/:entity/SearchLoan/:savedSearchName', [function (req, res, next) {
    let tenant = req.user.tenant;
    let savedSearchName = req.params.savedSearchName;
    let noPage = req.query.noPage;
    let sortOverride = req.body.sorts;
    let searchFilterViewOverride = req.body.searchFilterView;
    let searchParams;
    let entity = req.params.entity;

    req.store.dbAction(function* (db) {
        if (savedSearchName && savedSearchName.length) {
            return yield db.collection('saved-search')
                .findOne({ searchname: savedSearchName, TenantId: tenant });
        } else {
            res
                .status(404)
                .json({ message: 'Missing parameter - Saved Search Name' });
            return;
        }
    }).then(function (searchParams) {
        if (searchParams) {
            let replacementChars = searchParams.replacementChars;
            let replaceChar = searchParams.replaceChar;
            const replace = (str) => Object.keys(replacementChars).reduce((a, c) => a.replace(new RegExp(c, 'g'), replacementChars[c]), str);

            if (searchFilterViewOverride) { searchParams.searchFilterView = searchFilterViewOverride; }

            if (replaceChar) {
                searchParams.searchFilterView = searchParams.searchFilterView.reduce((a, c) => {
                    return [...a, normalize(c, replace)];
                }, []);
            }
            if (sortOverride) { searchParams.sorts = sortOverride; }
            appFilters.performLoanSearch(Object.assign(searchParams, { tenant: tenant, store: req.store, entity: entity, filterCollection: req.filterCollection, tagCollection: req.tagCollection }))
                .then(function (q) {
                    if (q) {
                        req.q = q;
                        if (noPage && noPage.length) {
                            q.toArray().then((intermediateResults) => {
                                if (intermediateResults && intermediateResults.length) {
                                    appFilters.mergeTagsWithLoanFilterRecord(req.store, intermediateResults, req.tagCollection)
                                        .then((mergedResults) => {
                                            res
                                                .status(200)
                                                .json(mergedResults);
                                        });
                                } else {
                                    res
                                        .status(404)
                                        .json({ message: 'Page you are looking for does not exists' });
                                }
                            });
                        } else {
                            next();
                        }
                    } else {
                        res
                            .status(404)
                            .json({ message: 'Page you are looking for does not exists' });
                    }
                })
                .catch((e) => {
                    res
                        .status(404)
                        .json({ message: 'Error occured while performing Search ' + savedSearchName + ' for tenant #' + tenant });
                });
        } else {
            res
                .status(404)
                .json({ message: 'Search ' + savedSearchName + ' does not exist for tenant #' + tenant });
            return;
        }
    });
}, paginate]);

router.get('/:entity/GetHistory/:entitytype/:entityid', function (req, res, next) {
    let entityType = req.params.entitytype;
    let entityId = req.params.entityid;
    let entity = req.params.entity;
    if (entityType && entityId) {
        function* doSearch() {
            const {
                'status-management': statusManagementEndpoint
            } = yield req.config('endpoints');

            try {
                const historyRecords = yield req.api.get(`${statusManagementEndpoint}/getstatushisotry/${req.params.entitytype}/${req.params.entityid}`);
                return historyRecords;
            } catch (error) {
                throw { code: 500, error }
            }
        }

        co(doSearch)
            .then((d) => {
                res
                    .status(200)
                    .json(d);
            })
            .catch((e) => {
                res
                    .status(500)
                    .json({ message: 'Error occured while fetching history : ' + e.message });
            });
    } else {
        res.status(404).json({ message: 'entitytype and entityid are required parameters' });
    }
});

router.get('/:entity/ExportData/:savedSearchName', [function (req, res, next) {
    let tenant = req.user.tenant;
    let savedSearchName = req.params.savedSearchName;
    let searchParams;
    let entity = req.params.entity;
    req.store.dbAction(function* (db) {
        if (savedSearchName && savedSearchName.length) {
            return yield db.collection('saved-search').findOne({ searchname: savedSearchName, TenantId: tenant });
        } else {
            res.status(404).json({ message: 'Missing parameter - Saved Search Name' });
            return;
        }
    }).then(function (searchParams) {
        if (searchParams) {
            let searchFilterViewOverride = searchParams.searchFilterView;
            let replacementChars = searchParams.replacementChars;
            let replaceChar = searchParams.replaceChar;
            const replace = (str) => Object.keys(replacementChars).reduce((a, c) => a.replace(new RegExp(c, 'g'), replacementChars[c]), str);

            if (searchFilterViewOverride) { searchParams.searchFilterView = searchFilterViewOverride; }

            if (replaceChar) {
                searchParams.searchFilterView = searchParams.searchFilterView.reduce((a, c) => {
                    return [...a, normalize(c, replace)];
                }, []);
            }
            appFilters.performSearch(Object.assign(searchParams, { tenant: tenant, store: req.store, entity: entity, filterCollection: req.filterCollection, tagCollection: req.tagCollection })).then(function (q) {
                if (q) {
                    req.q = q;
                    req.savedSearchName = savedSearchName;
                    req.exportFields = searchParams.exportFields;
                    req.exportFieldNames = searchParams.exportFieldNames;
                    next();
                } else {
                    res.status(404).json({ message: 'Page you are looking for does not exists' });
                }
            }).catch((e) => {
                res.status(404).json({ message: 'Error occured while exporting saved search ' + savedSearchName + ' data for tenant #' + tenant });
            });
        } else {
            res.status(404).json({ message: 'Search ' + savedSearchName + ' does not exist for tenant #' + tenant });
            return;
        }
    });
}, exportData]);

router.post('/:entity/ExportData/:savedSearchName', [function (req, res, next) {
    let tenant = req.user.tenant;
    let savedSearchName = req.params.savedSearchName;
    let searchParams;
    let searchFilterViewOverride = req.body.searchFilterView;
    let entity = req.params.entity;
    req.store.dbAction(function* (db) {
        if (savedSearchName && savedSearchName.length) {
            return yield db.collection('saved-search').findOne({ searchname: savedSearchName, TenantId: tenant });
        } else {
            res.status(404).json({ message: 'Missing parameter - Saved Search Name' });
            return;
        }
    }).then(function (searchParams) {
        if (searchParams) {
            let replacementChars = searchParams.replacementChars;
            let replaceChar = searchParams.replaceChar;
            const replace = (str) => Object.keys(replacementChars).reduce((a, c) => a.replace(new RegExp(c, 'g'), replacementChars[c]), str);

            if (searchFilterViewOverride) { searchParams.searchFilterView = searchFilterViewOverride; }
            console.log(searchParams);
            if (replaceChar) {
                searchParams.searchFilterView = searchParams.searchFilterView.reduce((a, c) => {
                    return [...a, normalize(c, replace)];
                }, []);
            }
            appFilters.performSearch(Object.assign(searchParams, { tenant: tenant, store: req.store, entity: entity, filterCollection: req.filterCollection, tagCollection: req.tagCollection })).then(function (q) {
                if (q) {
                    req.q = q;
                    req.savedSearchName = savedSearchName;
                    req.exportFields = searchParams.exportFields;
                    req.exportFieldNames = searchParams.exportFieldNames;
                    next();
                } else {
                    res.status(404).json({ message: 'Page you are looking for does not exists' });
                }
            }).catch((e) => {
                res.status(404).json({ message: 'Error occured while exporting saved search ' + savedSearchName + ' data for tenant #' + tenant });
            });
        } else {
            res.status(404).json({ message: 'Search ' + savedSearchName + ' does not exist for tenant #' + tenant });
            return;
        }
    });
}, exportData]);

router.post('/query/:e', function (req, res, next) {
    const { data } = req;
    if (data) {
        res.status(200).json(data);
    } else {
        res.status(404).json({ message: 'entitytype and entityid are required parameters' });
    }
});

module.exports = router;