'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');

const { healthcheck, token, setup } = require('lf-core-backend-extensions/lib/middleware');
var configuration = require('lf-core-backend-extensions/lib/api/configuration');
// var engine = require('./lib/core/engine/middleware');
const cors = require('cors');

var routes = require('./routes/index');

var app = express();

app.use(cors({
    origin: true,
    exposedHeaders: 'X-CURRENT-PAGE,X-PAGE-SIZE,X-TOTAL-COUNT,X-TOTAL-PAGES'
}));

function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        return false;
    }
    return compression.filter(req, res);
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '25mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression({ filter: shouldCompress }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//custom middleware
app.use(healthcheck);
app.use(token);
app.use(setup);

app.use(routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;